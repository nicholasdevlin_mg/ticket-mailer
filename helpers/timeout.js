function timeout(ms) {
	return new Promise(resolve =>
		setTimeout(() => {
			resolve(true);
		}, ms)
	);
}

module.exports = timeout;
