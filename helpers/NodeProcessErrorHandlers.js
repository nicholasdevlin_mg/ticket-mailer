require("dotenv").config();
const env = process.env.ENV;
const app = process.env.APP_NAME;
const { sendSlackErrorNotification } = require("./slackNotification");
const timeout = require("./timeout");

async function initialiseErrorHandlers() {
	process.on("unhandledRejection", async (error, promise) => {
		const errorObj = {
			app,
			environment: env,
			affected: error,
			message: promise,
		};
		if (env === "staging" || env === "production") {
			sendSlackErrorNotification(errorObj);
		} else {
			console.log(errorObj);
		}
	});
	process.on("warning", warning => {
		const errorObj = {
			app,
			environment: env,
			affected: warning.stack,
			message: warning.message,
		};
		if (env === "staging" || env === "production") {
			sendSlackErrorNotification(errorObj);
		} else {
			console.log(errorObj);
		}
	});
	// console.clear();
	console.log("\nReticulating Error Handlers...");
	await timeout(1500);
}

module.exports = initialiseErrorHandlers;
