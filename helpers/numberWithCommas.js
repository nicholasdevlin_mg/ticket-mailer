function numberWithCommas(x, notFixed) {
	const n = parseFloat(x, 10).toFixed(notFixed ? 0 : 2);
	return n.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

module.exports = numberWithCommas;
