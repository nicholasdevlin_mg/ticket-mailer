require("dotenv").config();
const requestPromise = require("request-promise");
const { WebClient } = require("@slack/web-api");
const timeout = require("./timeout");

const slackToken = process.env.SLACK_TOKEN;
const env = process.env.ENV;
const app = process.env.APP_NAME;
const slackWeb = new WebClient(slackToken);
const slackDevopsChannelId = "CJA058REX";
const slackErrorsChannelId = {
	development: "CKEHH6BQT",
	staging: "CKEHH6BQT",
	production: "CKEH2UGRY",
};
const slackReportsChannelId = {
	development: "CS9BG0KL3",
	staging: "CS9BG0KL3",
	production: "CS81LEM0S",
};
const slackBotQ = "https://api.forismatic.com/api/1.0/?method=getQuote&lang=en&format=json";

async function sendSlackNotification() {
	const hostUrl = process.env.API_URL;
	function getQ() {
		const OPTIONS = {
			uri: slackBotQ,
			json: true,
		};
		const qResponse = requestPromise(OPTIONS).then(response => {
			return response.quoteText;
		});
		return qResponse;
	}
	async function sendNotification() {
		const quote = (await getQ()) || "";
		await timeout(2000);
		const slackText = `${quote}\n\nYour latest ${env} build for ${app} is now running on ${hostUrl}`;
		await slackWeb.chat.postMessage({
			channel: slackDevopsChannelId,
			text: slackText,
		});
		console.log(`Slack notification to #DevOps sent.`);
	}
	if (env === "staging") {
		sendNotification();
	}
}

async function sendSlackErrorNotification(error) {
	const slackText = `YOU SHALL NOT PASS!\n\nError within ${
		process.env.APP_NAME
	} ${error.environment.toUpperCase()} environment.\n\n${error.affected}`;
	await slackWeb.chat.postMessage({
		channel: slackErrorsChannelId[error.environment],
		text: slackText,
	});
	console.log(`Slack notification to #DevOps sent.`);
}

async function sendSlackBugReportNotification(body) {
	const payload = JSON.parse(body);
	const companyName = payload.fields.summary;
	const description = payload.fields.description.content[0].content[0].text;
	const slackText = `Do not weep for all tears are not evil. A new bug has been reported, please check Jira for further information.\n\nCompany Name: ${companyName}\n\n${description}`;
	await slackWeb.chat.postMessage({
		channel: slackErrorsChannelId[env],
		text: slackText,
	});
	console.log(`Slack notification to #DevOps sent.`);
}

async function sendSlackErrorReactNotification(error) {
	const slackText = `YOU SHALL NOT PASS!\n\nError within ${
		error.app
	} ${error.environment.toUpperCase()} environment.\n\n${error.title}\n\n${error.description}\n\n${
		error.company
	}\n\n${error.route}`;
	return await slackWeb.chat.postMessage({
		channel: slackErrorsChannelId[error.environment],
		text: slackText,
	});
}

async function sendSlackReport(report) {
	return await slackWeb.chat.postMessage({
		channel: slackReportsChannelId[env],
		text: report,
	});
}

module.exports = {
	sendSlackNotification,
	sendSlackErrorNotification,
	sendSlackBugReportNotification,
	sendSlackErrorReactNotification,
	sendSlackReport,
};
