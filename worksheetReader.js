const Excel = require("exceljs");
const numberWithCommas = require("./helpers/numberWithCommas");

async function processBookingSpreadsheet(flightDate) {
	console.log(`Loading ${flightDate} tracking log...`);
	const completedId = [];
	const workbook1 = new Excel.Workbook();
	await workbook1.xlsx.readFile(`./${flightDate}-tracking.xlsx`);
	const worksheet1 = workbook1.getWorksheet(1);
	worksheet1.eachRow(row => {
		if (row.number >= 2) {
			const id = row.getCell(1).value;
			const sent = row.getCell(2).value;
			if (sent) {
				completedId.push(id);
			}
		}
	});

	console.log("Ticket IDs already sent:", completedId);

	console.log("Checking and preparing emails to be sent...");
	const workbook2 = new Excel.Workbook();
	const emailInjectionData = [];
	let totalPaxSydneyFlight = 0;
	let totalPaxBrisbaneFlight = 0;
	let totalPaxPerthFlight = 0;
	await workbook2.xlsx.readFile(`./${flightDate}-bookings.xlsx`).then(async worksheet => {
		const bookings = worksheet.worksheets[0];
		await bookings.eachRow((row, num) => {
			// ignores first header line in spreadsheet
			if (row.number >= 2) {
				function truncatePassportString(passport) {
					let string = passport;
					if (passport === "Passport: Indian Passport (Visa details required)") {
						string = "Passport: Indian Passport";
					}
					if (passport === "Passport: New Zealand Passport (Visa details required)") {
						string = "Passport: New Zealand";
					}
					return string;
				}

				let processedDate = new Date();
				const dd = String(processedDate.getDate()).padStart(2, "0");
				const mm = String(processedDate.getMonth() + 1).padStart(2, "0");
				const yyyy = processedDate.getFullYear();
				processedDate = dd + "/" + mm + "/" + yyyy;
				let flightNumber = "TBA";
				let flightDepartureDate = "TBA";
				let flightDepartureTime = "TBA";
				let flightArrivalTime = "TBA";
				let flightDepartureAirport = "TBA";
				let flightArrivalAirport = "TBA";
				const flightDescription = row.getCell(2).value;
				const surname = row.getCell(5).value;
				const givenNames = row.getCell(6).value;
				const phoneMobileNumber = row.getCell(7).value;
				const whatsAppNumber = row.getCell(8).value;
				const email = row.getCell(9).value;
				const passengerList = row.getCell(10).value;
				const splitPassengerList = [];
				const individualPassenger = passengerList.split("\n");
				individualPassenger.forEach(string => {
					const individualInfo = string.split(", ");
					const passengerDetail = {
						surname: individualInfo[0],
						givenNames: individualInfo[1],
						dob: individualInfo[3],
						passport: truncatePassportString(individualInfo[5]),
						visaClass: individualInfo[8],
						visaNumber: individualInfo[7],
					};
					splitPassengerList.push(passengerDetail);
				});
				const passengerTotal = row.getCell(11).value;
				const costPp = numberWithCommas(row.getCell(12).value);
				const totalCost = numberWithCommas(row.getCell(13).value);
				const worksheetId = row.getCell(31).value;
				const submissionId = `${num}-${worksheetId.result}`;

				if (flightDescription === "Delhi - Brisbane (4/5/2020)") {
					flightNumber = "JT2846";
					flightDepartureDate = "4/5/2020";
					flightDepartureTime = "11:00";
					flightDepartureAirport = "DEL - Delhi";
					flightArrivalTime = "07:15";
					flightArrivalAirport = "BNE - Brisbane";
					totalPaxBrisbaneFlight = totalPaxBrisbaneFlight + passengerTotal;
				}
				if (flightDescription === "Delhi - Perth (5/5/2020)") {
					flightNumber = "JP2850";
					flightDepartureDate = "5/5/2020";
					flightDepartureTime = "11:00";
					flightDepartureAirport = "DEL - Delhi";
					flightArrivalTime = "01:30";
					flightArrivalAirport = "PER - Perth";
					totalPaxPerthFlight = totalPaxPerthFlight + passengerTotal;
				}
				if (flightDescription === "Delhi - Sydney (6/5/2020)") {
					flightNumber = "JP2850";
					flightDepartureDate = "6/5/2020";
					flightDepartureTime = "11:00";
					flightDepartureAirport = "DEL - Delhi";
					flightArrivalTime = "07:30";
					flightArrivalAirport = "SYD - Sydney";
					totalPaxSydneyFlight = totalPaxSydneyFlight + passengerTotal;
				}
				const completeEmailData = {
					flightDescription,
					flightNumber,
					flightDepartureDate,
					flightDepartureTime,
					flightDepartureAirport,
					flightArrivalTime,
					flightArrivalAirport,
					processedDate,
					submissionId,
					surname,
					givenNames,
					phoneMobileNumber,
					whatsAppNumber,
					email,
					splitPassengerList,
					passengerTotal,
					costPp,
					totalCost,
				};
				const hasThisEmailBeenSent = completedId.filter(id => id === submissionId);
				if (hasThisEmailBeenSent.length === 1) {
				} else {
					console.log(`${submissionId} adding to send list...`);
					emailInjectionData.push(completeEmailData);
				}
			}
		});
	});

	console.log(`Total PAX count for Dehli - Brisbane Flight is ${totalPaxBrisbaneFlight}.`);
	console.log(`Total PAX count for Dehli - Sydney Flight is ${totalPaxSydneyFlight}.`);
	console.log(`Total PAX count for Dehli - Perth Flight is ${totalPaxPerthFlight}.`);
	console.log(`${emailInjectionData.length} email(s) required to be sent for ${flightDate}.`);
	return emailInjectionData;
}

async function processMultiBookingSpreadsheet(flightDate) {
	console.log(`Loading ${flightDate} tracking log...`);
	const completedId = [];
	const workbook1 = new Excel.Workbook();
	await workbook1.xlsx.readFile(`./${flightDate}-tracking.xlsx`);
	const worksheet1 = workbook1.getWorksheet(1);
	worksheet1.eachRow(row => {
		if (row.number >= 2) {
			const id = row.getCell(1).value;
			const sent = row.getCell(2).value;
			if (sent) {
				completedId.push(id);
			}
		}
	});

	console.log("Ticket IDs already sent:", completedId);

	console.log("Checking and preparing emails to be sent...");
	const workbook2 = new Excel.Workbook();
	const emailInjectionData = [];

	let totalDeparturePax0105Brisbane = 0;
	let totalDeparturePax0105Auckland = 0;
	let totalDeparturePax0105Sydney = 0;
	let totalDeparturePax0105Jakarta = 0;
	let totalDeparturePax0205Johannesburg = 0;
	let totalDeparturePax0305Jakarta = 0;
	let totalDeparturePax0405Sydney = 0;

	let totalArrivalPax0105Auckland = 0;
	let totalArrivalPax0105Sydney = 0;
	let totalArrivalPax0105Jakarta = 0;
	let totalArrivalPax0205Johannesburg = 0;
	let totalArrivalPax0305Jakarta = 0;
	let totalArrivalPax0405Sydney = 0;
	let totalArrivalPax0405Auckland = 0;

	await workbook2.xlsx.readFile(`./${flightDate}-bookings.xlsx`).then(async worksheet => {
		const bookings = worksheet.worksheets[0];
		await bookings.eachRow((row, num) => {
			// ignores first header line in spreadsheet
			if (row.number >= 2) {
				function truncatePassportString(passport) {
					let string = passport;
					if (passport === "Passport: Indian Passport (Visa details required)") {
						string = "Passport: Indian Passport";
					}
					if (passport === "Passport: New Zealand Passport (Visa details required)") {
						string = "Passport: New Zealand";
					}
					return string;
				}

				let processedDate = new Date();
				const dd = String(processedDate.getDate()).padStart(2, "0");
				const mm = String(processedDate.getMonth() + 1).padStart(2, "0");
				const yyyy = processedDate.getFullYear();
				processedDate = dd + "/" + mm + "/" + yyyy;
				let flightNumber = "TBA";
				const flightDescriptionDeparture = row.getCell(2).value;
				const flightDescriptionArrival = row.getCell(3).value;
				const flightDepartureDate = flightDescriptionDeparture.split("-");
				const flightDepartureAirport = flightDepartureDate[1].split("(");
				const flightDepartureTime = flightDepartureAirport[1].split(")");
				const flightArrivalDate = flightDescriptionArrival.split("-");
				const flightArrivalAirport = flightArrivalDate[1].split("(");
				const flightArrivalTime = flightArrivalAirport[1].split(")");
				const surname = row.getCell(4).value;
				const givenNames = row.getCell(5).value;
				const phoneMobileNumber = row.getCell(6).value;
				const whatsAppNumber = row.getCell(7).value;
				const email = row.getCell(8).value;
				// const emergencyContactSurname = row.getCell(9).value;
				// const emergencyContactGivenNames = row.getCell(10).value;
				// const emergencyContactphoneMobileNumber = row.getCell(11).value;
				// const emergencyContactwhatsAppNumber = row.getCell(12).value;
				// const emergencyContactEmail = row.getCell(13).value;
				const passengerList = row.getCell(14).value;
				const splitPassengerList = [];
				const individualPassenger = passengerList.split("\n");
				individualPassenger.forEach(string => {
					const individualInfo = string.split(", ");
					const passengerDetail = {
						surname: individualInfo[0],
						givenNames: individualInfo[1],
						dob: individualInfo[3],
						passport: truncatePassportString(individualInfo[5]),
						visaClass: individualInfo[8],
						visaNumber: individualInfo[7],
					};
					splitPassengerList.push(passengerDetail);
				});
				const passengerTotal = row.getCell(15).value;
				const costPp = numberWithCommas(row.getCell(16).value);
				const totalCost = numberWithCommas(row.getCell(17).value);
				const worksheetId = row.getCell(35).value;
				const submissionId = `${num}-${worksheetId.result}`;

				switch (flightDescriptionArrival) {
					case "Friday 1st May - Arrival Auckland International Terminal AKL (1500)":
						totalArrivalPax0105Auckland = totalArrivalPax0105Auckland + passengerTotal;
						break;
					case "Friday 1st May - Arrival Sydney Kingsford Smith International Terminal SYD (1735)":
						totalArrivalPax0105Sydney = totalArrivalPax0105Sydney + passengerTotal;
						break;
					case "Saturday 2nd May - Arrival Jakarta Soekarno-Hatta International Airport CGK (0100)":
						totalArrivalPax0105Jakarta = totalArrivalPax0105Jakarta + passengerTotal;
						break;
					case "Saturday 2nd May - Arrival Johannesburg O.R. Tambo International Airport JNB (1050)":
						totalArrivalPax0205Johannesburg = totalArrivalPax0205Johannesburg + passengerTotal;
						break;
					case "Sunday 3rd May - Arrival Jakarta Soekarno-Hatta International Airport CGK (2100)":
						totalArrivalPax0305Jakarta = totalArrivalPax0305Jakarta + passengerTotal;
						break;
					case "Monday 4th May - Arrival Sydney Kingsford Smith International Terminal SYD (1005)":
						totalArrivalPax0405Sydney = totalArrivalPax0405Sydney + passengerTotal;
						break;
					case "Monday 4th May - Arrival Auckland International Terminal AKL (1705)":
						totalArrivalPax0405Auckland = totalArrivalPax0405Auckland + passengerTotal;
						break;

					default:
				}

				switch (flightDescriptionDeparture) {
					case "Friday 1st May - Depart Auckland International Terminal AKL (1600)":
						flightNumber = "JT2882";
						totalDeparturePax0105Auckland = totalDeparturePax0105Auckland + passengerTotal;
						break;
					case "Friday 1st May - Depart Sydney Kingsford Smith International Terminal SYD (1900)":
						flightNumber = "JT2880";
						totalDeparturePax0105Sydney = totalDeparturePax0105Sydney + passengerTotal;
						break;
					case "Saturday 2nd May - Depart Jakarta Soekarno Hatta International Airport CGK (0230)":
						flightNumber = "JT2887";
						totalDeparturePax0105Jakarta = totalDeparturePax0105Jakarta + passengerTotal;
						break;
					case "Saturday 2nd May - Depart Denpasar Ngurah Rai International Airport DPS (0030)":
						flightNumber = "JT2887";
						break;
					case "Sunday 3rd May - Depart Johannesburg O.R. Tambo International Airport JNB (0230)":
						flightNumber = "JT2886";
						totalDeparturePax0205Johannesburg = totalDeparturePax0205Johannesburg + passengerTotal;
						break;
					case "Sunday 3rd May - Depart Jakarta Soekarno Hatta International Airport CGK (2330)":
						flightNumber = "JT2881";
						totalDeparturePax0305Jakarta = totalDeparturePax0305Jakarta + passengerTotal;
						break;
					case "Monday 4th May - Depart Sydney Kingsford Smith International Terminal SYD (1130)":
						flightNumber = "JT2883 ";
						totalDeparturePax0405Sydney = totalDeparturePax0405Sydney + passengerTotal;
						break;

					default:
						flightNumber = "TBA";
				}

				const completeEmailData = {
					flightNumber,
					flightDepartureDate: flightDepartureDate[0],
					flightDepartureTime: flightDepartureTime[0],
					flightDepartureAirport: flightDepartureAirport[0],
					flightArrivalDate: flightArrivalDate[0],
					flightArrivalTime: flightArrivalTime[0],
					flightArrivalAirport: flightArrivalAirport[0],
					processedDate,
					submissionId,
					surname,
					givenNames,
					phoneMobileNumber,
					whatsAppNumber,
					email,
					splitPassengerList,
					passengerTotal,
					costPp,
					totalCost,
				};
				const hasThisEmailBeenSent = completedId.filter(id => id === submissionId);
				if (hasThisEmailBeenSent.length === 1) {
				} else {
					console.log(`${submissionId} adding to send list...`);
					emailInjectionData.push(completeEmailData);
				}
			}
		});
	});
	console.log(
		`Friday 1st May - Depart Auckland total PAX count is ${totalDeparturePax0105Auckland}.`
	);
	console.log(
		`Friday 1st May - Depart Sydney total PAX count is ${
			totalArrivalPax0105Sydney + totalDeparturePax0105Sydney - totalArrivalPax0105Sydney
		}.`
	);
	console.log(
		`Friday 1st May - Depart Jakarta total PAX count is ${
			totalDeparturePax0105Jakarta === 0 && totalArrivalPax0105Jakarta === 0
				? "has no changes from above or is 0"
				: totalArrivalPax0105Jakarta + totalDeparturePax0105Jakarta - totalArrivalPax0105Jakarta
		}.`
	);
	console.log(
		`Saturday 2nd May - Depart Johannesburg total PAX count is ${
			totalArrivalPax0205Johannesburg +
			totalDeparturePax0205Johannesburg -
			totalArrivalPax0205Johannesburg
		}.`
	);
	console.log(
		`Sunday 3rd May - Depart Jakarta total PAX count is ${
			totalArrivalPax0305Jakarta === 0
				? "has no changes from above or is 0"
				: totalArrivalPax0305Jakarta + totalDeparturePax0305Jakarta - totalArrivalPax0305Jakarta
		}.`
	);
	console.log(
		`Monday 4th May - Depart Sydney total PAX count is ${
			totalArrivalPax0405Sydney === 0
				? "has no changes from above or is 0"
				: totalArrivalPax0405Sydney + totalDeparturePax0405Sydney - totalArrivalPax0405Sydney
		}.`
	);

	console.log(`${emailInjectionData.length} email(s) required to be sent for ${flightDate}.`);
	return emailInjectionData;
}

module.exports = { processBookingSpreadsheet, processMultiBookingSpreadsheet };
