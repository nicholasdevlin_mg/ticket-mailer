const nodemailer = require("nodemailer");
const Excel = require("exceljs");
const wkhtmltopdf = require("wkhtmltopdf");
const asyncForEach = require("../helpers/asyncForEach");
const prompt = require("prompt");
const mg = require("nodemailer-mailgun-transport");
const fs = require("fs");
const Handlebars = require("handlebars");
const mjml2html = require("mjml");
const auth = mg({
	auth: {
		api_key: "145a4496fab6ff4f9a1fdb805472ae7f-af6c0cec-0e0fefec",
		domain: "monarcglobal.com",
	},
});

async function AddToTrackingLog(data, flightDate) {
	console.log(`Adding ${data.submissionId} to tracking list...`);
	const workbook = new Excel.Workbook();
	await workbook.xlsx.readFile(`./${flightDate}-tracking.xlsx`);
	const worksheet = workbook.getWorksheet(1);
	worksheet.columns = [
		{ header: "id", key: "id", width: 10 },
		{ header: "sent", key: "sent", width: 10 },
	];
	await worksheet.addRow({ id: data.submissionId, sent: true });
	await workbook.xlsx.writeFile(`./${flightDate}-tracking.xlsx`);
}

// async function generateKTC() {
// 	console.log(`Generating KTC pdf...`);
// 	function mjml2Html(template, data) {
// 		const content = template(data),
// 			htmlOut = mjml2html(content);
// 		return htmlOut.html;
// 	}
// 	const body = fs.readFileSync(`./emails/templates/ktcGeneric.mjml`, "utf8");

// 	await Handlebars.registerHelper("each", function (context, options) {
// 		let ret = "";
// 		for (let i = 0, j = context.length; i < j; i++) {
// 			ret = ret + options.fn(context[i]);
// 		}
// 		return ret;
// 	});
// 	const bodyTemplate = await Handlebars.compile(body);
// 	const bodyHtml = await mjml2Html(bodyTemplate);
// 	// const bodyHtml = await mjml2Html(bodyTemplate, data);
// 	async function generatePDF() {
// 		return new Promise((resolve, reject) => {
// 			wkhtmltopdf(bodyHtml, { output: `./hc-message.pdf` }, function (err, stream) {
// 				if (err) console.log({ err });
// 				return resolve(stream);
// 			});
// 		});
// 	}
// 	await generatePDF();
// }

async function sendMultiFlightTicketEmail(data, flightDate) {
	console.log(`Sending ${data.submissionId} ticket to ${data.email}...`);
	function mjml2Html(template, data) {
		const content = template(data),
			htmlOut = mjml2html(content);
		return htmlOut.html;
	}
	const quote = fs.readFileSync(`./emails/templates/${flightDate}.mjml`, "utf8");
	const body = fs.readFileSync(`./emails/templates/ticketBodyGeneric.mjml`, "utf8");

	await Handlebars.registerHelper("each", function (context, options) {
		let ret = "";
		for (let i = 0, j = context.length; i < j; i++) {
			ret = ret + options.fn(context[i]);
		}
		return ret;
	});
	const template = await Handlebars.compile(quote);
	const bodyTemplate = await Handlebars.compile(body);
	const html = await mjml2Html(template, data);
	const bodyHtml = await mjml2Html(bodyTemplate, data);
	async function generatePDF() {
		return new Promise((resolve, reject) => {
			wkhtmltopdf(html, { output: `./${flightDate}/${data.submissionId}.pdf` }, function (
				err,
				stream
			) {
				if (err) console.log({ err });
				return resolve(stream);
			});
		});
	}
	await generatePDF();
	const transport = await nodemailer.createTransport(auth);
	return new Promise((resolve, reject) => {
		transport.sendMail(
			{
				html: bodyHtml,
				from: "sales@monarcglobal.com",
				// to: "dev@monarcglobal.com",
				to: data.email,
				bcc: "dev@monarcglobal.com",
				// An array if you have multiple recipients.
				subject: `Electronic Ticket - #${data.submissionId}`,
				text: "",
				attachments: [
					{
						filename: `${data.submissionId}.pdf`,
						path: fs.createReadStream(`./${flightDate}/${data.submissionId}.pdf`),
					},
				],
			},
			(err, info) => {
				if (err) {
					console.log(err);
					process.exit();
				} else {
					console.log("Success!");
					resolve();
				}
			}
		);
	});
}

async function sendFlightTicketEmail(data, flightDate) {
	console.log(`Sending ${data.submissionId} ticket to ${data.email}...`);
	function mjml2Html(template, data) {
		const content = template(data),
			htmlOut = mjml2html(content);
		return htmlOut.html;
	}
	const quote = fs.readFileSync(`./emails/templates/${flightDate}.mjml`, "utf8");
	const body = fs.readFileSync(`./emails/templates/ticketBody.mjml`, "utf8");

	await Handlebars.registerHelper("each", function (context, options) {
		let ret = "";
		for (let i = 0, j = context.length; i < j; i++) {
			ret = ret + options.fn(context[i]);
		}
		return ret;
	});
	const template = await Handlebars.compile(quote);
	const bodyTemplate = await Handlebars.compile(body);
	const html = await mjml2Html(template, data);
	const bodyHtml = await mjml2Html(bodyTemplate, data);
	async function generatePDF() {
		return new Promise((resolve, reject) => {
			wkhtmltopdf(html, { output: `./${flightDate}/${data.submissionId}.pdf` }, function (
				err,
				stream
			) {
				if (err) console.log({ err });
				return resolve(stream);
			});
		});
	}
	await generatePDF();
	const transport = await nodemailer.createTransport(auth);
	return new Promise((resolve, reject) => {
		transport.sendMail(
			{
				html: bodyHtml,
				from: "sales@monarcglobal.com",
				// to: "dev@monarcglobal.com",
				to: data.email,
				bcc: "dev@monarcglobal.com",
				// An array if you have multiple recipients.
				subject: `Electronic Ticket - #${data.submissionId}`,
				text: "",
				attachments: [
					{
						filename: `${data.submissionId}.pdf`,
						path: fs.createReadStream(`./${flightDate}/${data.submissionId}.pdf`),
					},
					{
						filename: `hc-message.pdf`,
						path: fs.createReadStream(`./${flightDate}/hc-message.pdf`),
					},
				],
			},
			(err, info) => {
				if (err) {
					console.log(err);
					process.exit();
				} else {
					console.log("Success!");
					resolve();
				}
			}
		);
	});
}

async function prepareMultiBookingEmails(data, flightDate) {
	prompt.start();
	console.log("Would you like to continue? Y/N");
	prompt.get(["send"], async function (err, result) {
		const answer = result.send.toLowerCase();
		if (answer === "n") {
			console.log("Quitting...");
			process.exit();
		}
		if (answer === "y") {
			console.log(`Preparing to send emails for ${flightDate}...`);
			asyncForEach(data, async emailData => {
				await sendMultiFlightTicketEmail(emailData, flightDate);
				await AddToTrackingLog(emailData, flightDate);
			});
		}
	});
}

async function prepareEmails(data, flightDate) {
	// await generateKTC();
	prompt.start();
	console.log("Would you like to continue? Y/N");
	prompt.get(["send"], async function (err, result) {
		const answer = result.send.toLowerCase();
		if (answer === "n") {
			console.log("Quitting...");
			process.exit();
		}
		if (answer === "y") {
			console.log(`Preparing to send emails for ${flightDate}...`);
			asyncForEach(data, async emailData => {
				await sendFlightTicketEmail(emailData, flightDate);
				await AddToTrackingLog(emailData, flightDate);
			});
		}
	});
}

module.exports = { prepareEmails, prepareMultiBookingEmails };
