const prompt = require("prompt");
const { processBookingSpreadsheet, processMultiBookingSpreadsheet } = require("./worksheetReader");
const { prepareEmails, prepareMultiBookingEmails } = require("./emails/email");

async function startApp() {
	function onErr(err) {
		console.log(err);
		process.exit();
	}
	prompt.start();
	console.log("Please enter flight date...");
	const flightDates = [
		"160420",
		"180420",
		"190420",
		"200420",
		"250420",
		"010520",
		"020520",
		"040520",
	];
	prompt.get(["flightDate"], async function (err, result) {
		const selectedFlightDate = flightDates.filter(flightDate => flightDate === result.flightDate);
		if (selectedFlightDate.length === 0) {
			err = "Flight Number invalid.";
		}
		if (err) {
			return onErr(err);
		}
		console.log("  Flight Date: " + result.flightDate);
		if (result.flightDate === "010520" || result.flightDate === "020520") {
			console.log("Multi-leg flight detected...");
			const emailData = await processMultiBookingSpreadsheet(result.flightDate);
			await prepareMultiBookingEmails(emailData, result.flightDate);
		} else {
			const emailData = await processBookingSpreadsheet(result.flightDate);
			await prepareEmails(emailData, result.flightDate);
		}
	});
}

startApp();
